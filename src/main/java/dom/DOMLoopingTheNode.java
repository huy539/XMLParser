package dom;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class DOMLoopingTheNode {
	public static void main(String[] args) {
		File file = new File("dom/staff.xml");
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();

			Document doc = builder.parse(file);

			System.out.println("Root element: "
					+ doc.getDocumentElement().getNodeName());
			if (doc.hasChildNodes()) {
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
