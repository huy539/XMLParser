package dom;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;

/**
 * Hello world!
 *
 */
public class DOMReadFileXML {
	public static void main(String[] args) {
		{
			File xmlFile = new File("dom/staff.xml");
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory
					.newInstance();
			try {
				DocumentBuilder builder = builderFactory.newDocumentBuilder();
				Document doc = builder.parse(xmlFile);

				NodeList nodeList = doc.getElementsByTagName("staff");
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					System.out.println("\n Current Element: "
							+ node.getNodeName());
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;

						System.out.println("Staff id :"
								+ element.getAttribute("id"));
						System.out.println("First Name: "
								+ element.getElementsByTagName("firstname")
										.item(0).getTextContent());
						System.out.println("Last Name:"
								+ element.getElementsByTagName("lastname")
										.item(0).getTextContent());
						System.out.println("Nick Name:"
								+ element.getElementsByTagName("nickname")
										.item(0).getTextContent());
						System.out.println("Salary: "
								+ element.getElementsByTagName("salary")
										.item(0).getTextContent());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
